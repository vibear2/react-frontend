# viBear's Products Inventory ReactJS frontend.

## Description

This is the frontend or view that interacts with viBear's graphql products inventory API and allows the user to perform the main CRUD operations.

## Main features

1. Components and overall UI optimized for small and large screens.
2. Syncronization on demand.
3. Individual and global search.

## Showcase

1. ivaPercentages
   1. Read (Same on both sizes)  
      ![](readme_assets/readIvaPercentages.png)
2. productTypes
   1. Create
      - Large screen  
        ![](readme_assets/productTypesCreate.png)
      - Small screen  
        ![](readme_assets/small_productTypesCreate.png)
   2. Read
      - Large screen
        ![](readme_assets/productTypes_read_large.png)
      - Small screen  
        ![](readme_assets/small_productTypesRead.png)
   3. Update
      - Large screen
        ![](readme_assets/productTypes_update_large.png)
      - Small screen
        ![](readme_assets/productTypes_update_small.png)
   4. Delete
      - Large screen
        ![](readme_assets/productTypes_delete_large.png)
      - Small screen
        ![](readme_assets/productTypes_delete_small.png)
3. products
   1. Create
      - Large screen
        ![](readme_assets/products_create_large.png)
      - Small screen
        ![](readme_assets/products_create_small.png)
   2. Read
      - Large screen
        ![](readme_assets/products_read_large.png)
      - Small screen
        ![](readme_assets/products_read_small.png)
   3. Update
      - Large screen
        ![](readme_assets/products_update_large.png)
      - Small screen
        ![](readme_assets/products_update_small.png)
   4. Delete
      - Large screen
        ![](readme_assets/products_delete_large.png)
      - Small screen
        ![](readme_assets/products_delete_small.png)
4. Syncronization on demand
   ![](readme_assets/sync.png)
5. Search
   - Individual
     Refers to search boxes included on dropdown menus.
   - Global
     Refers to search boxes included on manage views.

## Available environment variables with examples

This variables are meant to be placed in a .env file in the root of the project.

- REACT_APP_URL_API=http://localhost:4000/graphql/

## How to run

1. Install [Node JS](https://nodejs.org/) and [Git](https://git-scm.com/downloads)
2. Clone the repository

```bash
  git clone https://gitlab.com/vibear2/react-frontend.git
```

3. Install the project dependencies

```bash
npm install
```

4. Run the project

```bash
npm start
```

## References

- [Previous repository](https://github.com/MiguelRodrii/viBear-front)
