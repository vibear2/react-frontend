/*
MIT License

Copyright (c) 2021 Team SGSI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

import React, { useEffect, useRef } from "react";
import { Toast } from "primereact/toast";
//import "./styles.scss";
import { useDispatch, useSelector } from "react-redux";
import { hideToast } from "../redux/actions/toast";
export const ToastMessage = () => {
  const dispatch = useDispatch();
  const toast = useRef(null);
  const { isOpen, typeMessage, message } = useSelector((state) => state.toast);

  useEffect(() => {
    const showSuccess = () => {
      toast.current.show({
        severity: "success",
        summary: "Éxito",
        detail: message,
        life: 3000,
      });
    };

    const showInfo = () => {
      toast.current.show({
        severity: "info",
        summary: "Importante",
        detail: message,
        life: 3000,
      });
    };

    const showWarn = () => {
      toast.current.show({
        severity: "warn",
        summary: "Advertencia",
        detail: message,
        life: 3000,
      });
    };

    const showError = () => {
      toast.current.show({
        severity: "error",
        summary: "Error",
        detail: message,
        life: 3000,
      });
    };

    if (isOpen) {
      switch (typeMessage) {
        case "success":
          showSuccess();
          return;
        case "info":
          showInfo();
          return;
        case "warn":
          showWarn();
          return;
        case "error":
          showError();
          return;
        case "clear":
          clearToast();
          return;
        default:
      }
    }
  }, [isOpen, typeMessage, message]);

  const clearToast = () => {
    toast.current.clear();
  };
  const handleHideToast = () => {
    dispatch(hideToast());
  };
  return (
    <div>
      <Toast style={{width:"20em"}} ref={toast} onRemove={handleHideToast} />
    </div>
  );
};
