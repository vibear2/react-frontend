import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Panel } from 'primereact/panel';
import {toggleHelpPanel} from '../redux/actions/helpPanel.js';


export const HelpPanel = () => {
  const dispatch = useDispatch();
  const { isCollapsed, message, steps } = useSelector(state => state.helpPanel);

  return (<>
    <Panel className="p-mb-2" header="Ayuda" toggleable collapsed={isCollapsed} onToggle={() => {toggleHelpPanel(!isCollapsed)(dispatch)}}>
      <p>{message}</p>
      <ol>
        {steps.map((step, index) => <li key={index}>{step}</li>)}
      </ol>
    </Panel>
  </>);
};
