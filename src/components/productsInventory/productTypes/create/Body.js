import { Card } from "primereact/card";
import { InputText } from "primereact/inputtext";
import { InputSwitch } from "primereact/inputswitch";
import { Dropdown } from "primereact/dropdown";
import { Button } from "primereact/button";
import React, { useState, useEffect } from "react";
import { showToast } from "../../../../redux/actions/toast";
import { useDispatch, useSelector } from "react-redux";
import { getIvaPercentages } from "../../../../redux/actions/productsInventory/ivaPercentages";
import { createProductType } from "../../../../redux/actions/productsInventory/productTypes";
import { useDidMountEffect } from "../../../../hooks/useDidMountEffect.js";
import { updateHelpPanelContent } from '../../../../redux/actions/helpPanel.js';

export const Body = () => {
  const dispatch = useDispatch();

  const { ivaPercentages, message: ivaPercentagesMessage, loading: ivaPercentagesLoading, success: ivaPercentagesSuccess } = useSelector(
    (state) => state.productsInventory.ivaPercentages
  );
  const { message: productTypesMessage, loading: productTypesLoading, success: productTypesSuccess } = useSelector(
    (state) => state.productsInventory.productTypes
  );
  const { sync } = useSelector((state) => state.navigation.mainMenu);

  const [name, setName] = useState("");
  const [isExpirable, setIsExpirable] = useState(true);
  const [selectedIvaPercentageId, setselectedIvaPercentageId] =
    useState(undefined);

  useEffect(() => {
    updateHelpPanelContent(
      "Bienvenido/a a la vista para la creación de tipos de productos.",
      [
        "Rellene el campo Nombre de acuerdo al nombre del tipo de producto.",
        "Active o desactive el switch correspondiente a la perecibilidad del producto.",
        "Seleccione uno de los porcentajes válidos que aplique al tipo de producto.",
        "Haga click o tap en el botón Crear."
      ]
    )(dispatch)
    if (ivaPercentages === null) getIvaPercentages()(dispatch);
  }, []);

  useDidMountEffect(() => {
    getIvaPercentages()(dispatch);
    if (!ivaPercentagesLoading && ivaPercentagesSuccess) showToast("success", ivaPercentagesMessage)(dispatch);
  }, [sync]);
  useDidMountEffect(() => {
    if (!ivaPercentagesLoading && !ivaPercentagesSuccess) showToast("error", ivaPercentagesMessage)(dispatch);
  }, [ivaPercentagesLoading, ivaPercentagesSuccess]);
  useDidMountEffect(() => {
    if (!productTypesLoading && !productTypesSuccess) showToast("error", productTypesMessage)(dispatch);
    if (!productTypesLoading && productTypesSuccess) {
      showToast("success", productTypesMessage)(dispatch);
      setName("");
    }
  }, [productTypesLoading, productTypesSuccess]);

  const hadleSubmitCreateProductType = () => {
    if (name === "" || selectedIvaPercentageId === undefined) {
      showToast("warn", "Por favor, rellene todos los campos.")(dispatch);
      return;
    }
    createProductType(name, isExpirable, selectedIvaPercentageId)(dispatch);
  };

  return (
    <>
      <Card className="p-mt-2">
        <div className="p-field">
          <label className="p-d-block">Nombre</label>
          <InputText
            className="p-d-block"
            value={name}
            onChange={(e) => setName(e.target.value)}
            required={true}
          />
          <small className="p-d-block">
            Nombre del tipo de producto. Ejemplo: enlatados.
          </small>
        </div>

        <div className="p-field">
          <label htmlFor="productTypeIsExpirable" className="p-d-block">
            Es expirable
          </label>
          <InputSwitch
            checked={isExpirable}
            onChange={(e) => setIsExpirable(e.value)}
          />
          <small id="productTypeIsExpirable-help" className="p-d-block">
            Perecibilidad del producto.
          </small>
        </div>

        <div className="p-field">
          <label htmlFor="productTypeIva" className="p-d-block">
            Porcentaje de IVA aplicado
          </label>
          {ivaPercentages === null ? (
            <h1>Loading...</h1>
          ) : (
            <Dropdown
              value={selectedIvaPercentageId}
              options={ivaPercentages}
              onChange={(e) => {
                setselectedIvaPercentageId(e.value);
              }}
              optionLabel="value"
              optionValue="id"
            />
          )}
          <small id="productTypeIva-help" className="p-d-block">
            Porcentaje de IVA que se aplica al tipo de producto.
          </small>
        </div>

        <Button icon="pi pi-save" label="Crear" onClick={hadleSubmitCreateProductType} />
      </Card>
    </>
  );
};
