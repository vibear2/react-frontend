import * as helpPanelTypes from "../constants/helpPanelTypes.js";

const initialState = {
  isCollapsed: true,
  message: "",
  steps: []
};

export const helpPanelReducer = (state = initialState, action) => {
  switch (action.type) {
    case helpPanelTypes.TOGGLE_HELP_PANEL:
      return { ...state, isCollapsed: action.payload.isCollapsed };
    case helpPanelTypes.UPDATE_HELP_PANEL_CONTENT:
      return {
        ...state,
        message: action.payload.message,
        steps: action.payload.steps
      };
    default:
      return state;
  }
};
