import * as groupTypes from "../../constants/productsInventory/ivaPercentages.js";

const initialState = {
  loading: false,
  success: false,
  ivaPercentages: null,
  message: ""
};

export const ivaPercentagesReducer = (state = initialState, action) => {
  switch (action.type) {
    case groupTypes.GET_IVA_PERCENTAGES_LOADING:
      return { ...state, loading: action.payload.loading };
    case groupTypes.GET_IVA_PERCENTAGES_SUCCESS:
      return {
        ...state,
        loading: action.payload.loading,
        success: action.payload.success,
        ivaPercentages: action.payload.ivaPercentages,
        message: action.payload.message
      };
    case groupTypes.GET_IVA_PERCENTAGES_FAILED:
      return {
        ...state,
        loading: action.payload.loading,
        success: action.payload.success,
        message: action.payload.message
      };
    default:
      return { ...state };
  }
};
