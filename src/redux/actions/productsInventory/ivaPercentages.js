import API from "../../../services";
import { gql } from "graphql-request";
import * as groupTypes from "../../constants/productsInventory/ivaPercentages.js";
import { UNKNOWN_ERROR_OCCURED } from "../../../constants/exceptions.js";

export const getIvaPercentages = () => async (dispatch) => {
  try {
    dispatch({
      type: groupTypes.GET_IVA_PERCENTAGES_LOADING,
      payload: { loading: true },
    });

    const response = await API.request(gql`
      query {
        ivaPercentages {
          id
          value
        }
      }
    `);
    dispatch({
      type: groupTypes.GET_IVA_PERCENTAGES_SUCCESS,
      payload: {
        loading: false,
        success: true,
        ivaPercentages: response.ivaPercentages,
        message: "Porcentajes de IVA adquiridos de forma exitosa."
      },
    });
  } catch (error) {
    dispatch({
      type: groupTypes.GET_IVA_PERCENTAGES_FAILED,
      payload: {
        loading: false,
        success: false,
        message: error.response === undefined ? UNKNOWN_ERROR_OCCURED : error.response.errors[0].message.body === undefined ? UNKNOWN_ERROR_OCCURED : error.response.errors[0].message.body.nontechnical
      },
    });
    console.log(error);
  }
};