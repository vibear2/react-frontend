import API from "../../../services";
import { gql } from "graphql-request";
import * as groupTypes from "../../constants/productsInventory/productTypes.js";
import { UNKNOWN_ERROR_OCCURED } from "../../../constants/exceptions.js";

export const createProductType =
  (name, isExpirable, ivaPercentageId) => async (dispatch) => {
    try {
      dispatch({
        type: groupTypes.CREATE_PRODUCT_TYPE_LOADING,
        payload: { loading: true },
      });
      const response = await API.request(gql`
      mutation {
        createProductType(
          productType: { name: "${name}", is_expirable: ${isExpirable}, iva_percentage_id: ${ivaPercentageId} }
        ) {
          id
          name
          is_expirable
          iva_percentage {
            id
            value
          }
        }
      }
    `);
      dispatch({
        type: groupTypes.CREATE_PRODUCT_TYPE_SUCCESS,
        payload: {
          loading: false,
          success: true,
          createdProductType: response.createProductType,
          message: 'Tipo de producto ' + name + ' creado de forma exitosa.'
        },
      });
    } catch (error) {
      dispatch({
        type: groupTypes.CREATE_PRODUCT_TYPE_FAILED,
        payload: {
          loading: false,
          success: false,
          message: error.response === undefined ? UNKNOWN_ERROR_OCCURED : error.response.errors[0].message.body === undefined ? UNKNOWN_ERROR_OCCURED : error.response.errors[0].message.body.nontechnical
        },
      });
      console.log(error);
      // First conditional handles the runtime or non GraphQL errors, second conditional handles the GraphQL API unknown errors.
    }
  };

export const getProductTypes = () => async (dispatch) => {
  try {
    dispatch({
      type: groupTypes.GET_PRODUCT_TYPES_LOADING,
      payload: { loading: true },
    });
    const response = await API.request(gql`
      query {
        productTypes {
          id
          name
          is_expirable
          iva_percentage {
            id
            value
          }
        }
      }
    `);
    dispatch({
      type: groupTypes.GET_PRODUCT_TYPES_SUCCESS,
      payload: {
        loading: false,
        success: true,
        productTypes: response.productTypes,
        message: "Tipos de productos adquiridos de forma exitosa."
      },
    });
  } catch (error) {
    dispatch({
      type: groupTypes.GET_PRODUCT_TYPES_FAILED,
      payload: {
        loading: false,
        success: false,
        message: error.response === undefined ? UNKNOWN_ERROR_OCCURED : error.response.errors[0].message.body === undefined ? UNKNOWN_ERROR_OCCURED : error.response.errors[0].message.body.nontechnical
      },
    });
    console.log(error);
  }
};

export const deleteProductType = (productType) => async (dispatch) => {
  try {
    dispatch({
      type: groupTypes.DELETE_PRODUCT_TYPE_LOADING,
      payload: { loading: true },
    });
    const response = await API.request(gql`
      mutation DeleteProductType($id: Int!) {
        deleteProductType (id: $id) {
          id
        }
      }
    `, { id: productType.id });
    dispatch({
      type: groupTypes.DELETE_PRODUCT_TYPE_SUCCESS,
      payload: {
        loading: true,
        success: true,
        deletedProductTypeId: response.deleteProductType.id,
      },
    });
  } catch (error) {
    dispatch({
      type: groupTypes.DELETE_PRODUCT_TYPE_FAILED,
      payload: { loading: true, success: false },
    });
    // First if handles the runtime or non GraphQL errors
    if (error.response === undefined) {
      console.log(error);
      throw new Error(UNKNOWN_ERROR_OCCURED);
    }
    // Second if handles the GraphQL API unknown errors
    if (error.response.errors[0].message.body === undefined) {
      console.log(error);
      console.log(error.response);
      throw new Error(UNKNOWN_ERROR_OCCURED);
    }
    // Finally if the thrown error is a GraphQL API known one it is send to the uper layer to show a proper notification to the user.
    console.log(error.response);
    throw new Error(error.response.errors[0].message.body.nontechnical);
  }
};

export const updateProductType = (productType) => async (dispatch) => {
  try {
    dispatch({
      type: groupTypes.UPDATE_PRODUCT_TYPE_LOADING,
      payload: { loading: true },
    });
    const response = await API.request(gql`
      mutation UpdateProductType($id: Int!, $name: String, $is_expirable: Boolean, $iva_percentage_id: Int) {
        updateProductType(
          id: $id
          productType: {
            name: $name
            is_expirable: $is_expirable
            iva_percentage_id: $iva_percentage_id
          }
        ) {
          id
          name
          is_expirable
          iva_percentage {
            id
            value
          }
        }
      }
    `, {
      id: productType.id,
      name: productType.name,
      is_expirable: productType.is_expirable,
      iva_percentage_id: productType.iva_percentage.id
    });
    dispatch({
      type: groupTypes.UPDATE_PRODUCT_TYPE_SUCCESS,
      payload: {
        loading: true,
        success: true,
        updateProductType: response.updateProductType,
      },
    });
  } catch (error) {
    dispatch({
      type: groupTypes.UPDATE_PRODUCT_TYPE_FAILED,
      payload: { loading: true, success: false },
    });
    // First if handles the runtime or non GraphQL errors
    if (error.response === undefined) {
      console.log(error);
      throw new Error(UNKNOWN_ERROR_OCCURED);
    }
    // Second if handles the GraphQL API unknown errors
    if (error.response.errors[0].message.body === undefined) {
      console.log(error);
      console.log(error.response);
      throw new Error(UNKNOWN_ERROR_OCCURED);
    }
    // Finally if the thrown error is a GraphQL API known one it is send to the uper layer to show a proper notification to the user.
    console.log(error.response);
    throw new Error(error.response.errors[0].message.body.nontechnical);
  }
};