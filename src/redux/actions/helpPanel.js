import * as helpPanelTypes from "../constants/helpPanelTypes.js";

export const toggleHelpPanel = (isCollapsed) => (dispatch) => {
    try {
        dispatch(
            { type: helpPanelTypes.TOGGLE_HELP_PANEL, payload: { isCollapsed: isCollapsed } }
        );
    } catch (error) {
        console.log(error);
    }
};

export const updateHelpPanelContent = (message, steps) => (dispatch) => {
    try {
        dispatch(
            { type: helpPanelTypes.UPDATE_HELP_PANEL_CONTENT, payload: { message: message, steps: steps } }
        );
    } catch (error) {
        console.log(error);
    }
};